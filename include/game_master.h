#ifndef GAME_MASTER_H
#define GAME_MASTER_H

#include "defines.h"
#include "functions.h"
#include "board.h"
#include "player.h"

#include <stdio.h>
#include <time.h>
#include <stdlib.h>

void play(board_t* da_board, player_t* player1, player_t* player2);
void tournament(player_t* player1, player_t* player2, int nbr_games, int board_size);


#endif //GAME_MASTER_H
