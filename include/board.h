#ifndef BOARD_H
#define BOARD_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "defines.h"
#include "functions.h"

typedef struct board board_t;

board_t* make_board(int size);
board_t* copy_board(board_t* da_board);
void free_board(board_t* board);

void reset_starting_state(board_t* da_board, char color_p1, char color_p2);
void reset_starting_state_while_staying_symetrical(board_t* da_board, char char_p1, char char_p2);

int get_size(board_t* da_board);
char get_cell(board_t* da_board, int x, int y);
int is_board_full(board_t* da_board, char player1, char player2);
int who_is_the_winner_half(board_t* da_board, char player1, char player2);
int who_is_the_winner_full(board_t* da_board, char player, char player2);
int how_many_cells_are_this_color(board_t* da_board, char color);
int how_many_boxes_can_be_reached_by_this_char(board_t* da_board, char color_player, char aimed_char);

void update_world(board_t* board, char player, char move);

void print_board(board_t* da_board);
void print_percentage_occupation(board_t* da_board, char color_p1, char color_p2);


#endif
