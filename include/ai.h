#ifndef AI_H
#define AI_H

#include "defines.h"
#include "functions.h"
#include "board.h"

#include <stdio.h>
#include <time.h>
#include <stdlib.h>

char ask_AI_random_not_stupid(board_t* da_board, char char_player);
char ask_AI_random();
char ask_AI_greedy(board_t* da_board, char player_char);
char ask_AI_thoughtfull_greedy(board_t* da_board, char player_char);

#endif //Ai_H
