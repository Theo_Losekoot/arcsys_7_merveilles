#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include "defines.h"

#include <stdio.h>
#include <time.h>
#include <stdlib.h>

void clear_buffer();
int get_number_between_0_and_x_on_keyboard(int x);
char get_char_from_keyboard_input();
void print_char_autocolor(char letter, char letter_p1, char letter_p2);

#endif //FUNCTIONS_H
