#ifndef DEFINES_H
#define DEFINES_H

#define NB_COLORS 7

#define BOLD "\x1b[1m"

#define GREEN_BG        "\x1b[42m"
#define CYAN_BG      "\x1b[46m"

#define BLACK   "\x1b[30m"
#define RED     "\x1b[31m"
#define GREEN   "\x1b[32m"
#define YELLOW  "\x1b[33m"
#define BLUE    "\x1b[34m"
#define MAGENTA "\x1b[35m"
#define CYAN    "\x1b[36m"
#define WHITE   "\x1B[37m"
#define ANSI_COLOR_RESET   "\x1b[0m"

#endif //DEFINES_H
