#ifndef PLAYER_H
#define PLAYER_H

#include <stdio.h>
#include <stdlib.h>

#include "defines.h"
#include "functions.h"
#include "board.h"
#include "ai.h"

typedef struct player player_t;

player_t* make_player(char char_player, int role);
void free_player(player_t* da_player);
char get_char_player(player_t* da_player);
void play_turn(board_t* da_board, player_t* da_player, player_t* da_opponent, int is_player1_turn);
void show_possible_roles();

enum ROLES
{
	HUMAN = 0,
	AI_RANDOM = 1,
	AI_RANDOM_NOT_STUPID = 2,
	AI_GREEDY = 3,
	AI_BETTER_GREEDY = 4
};



#endif //PLAYER_H
