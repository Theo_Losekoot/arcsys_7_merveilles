#include <stdio.h>
#include <time.h>
#include <stdlib.h>


#include "include/board.h"
#include "include/player.h"
#include "include/game_master.h"

void ask_for_parameters(int choices[])
{
	show_possible_roles();
	printf("Choix du premier joueur : ");
	choices[0] = get_number_between_0_and_x_on_keyboard(AI_BETTER_GREEDY);
	printf("Choix du deuxième joueur : ");
	choices[1] = get_number_between_0_and_x_on_keyboard(AI_BETTER_GREEDY);
	printf("Tournoi ou jeu simple ? (0 pour tournoi, 1 pour jeu simple) : ");
	choices[2] = get_number_between_0_and_x_on_keyboard(1);
	printf("Taille de la grille (< 1000000) : ");
	choices[3] = get_number_between_0_and_x_on_keyboard(999999);
}

// Asks the user for parameters then calls the relevant functions
int main()
{
	srand(time(NULL));
	printf("Bienvenue dans 7 colors \n");
	int continue_loop = 1;

	while (continue_loop)
	{
		char char_p1 = '^';
		char char_p2 = 'v';
		int user_input[4];
		ask_for_parameters(user_input);
		int type_p1 = user_input[0];
		int type_p2 = user_input[1];
		int play_type = user_input[2];
		int size_board = user_input[3];

		if(play_type)
		{
			player_t* player1 = make_player(char_p1, type_p1);
			player_t* player2 = make_player(char_p2, type_p2);

			board_t* da_board = make_board(size_board);

			play(da_board, player1, player2);
		}
		else
		{
			player_t* player1 = make_player(char_p1, type_p1);
			player_t* player2 = make_player(char_p2, type_p2);
			tournament(player1, player2, 100, size_board);
		}
		printf("\nOn continue ? (0 pour non, 1 pour oui) : ");
		continue_loop = get_number_between_0_and_x_on_keyboard(1);
	}

	return 0;
}
