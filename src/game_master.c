#include "../include/game_master.h"



int play_game(board_t* da_board, player_t* player1, player_t* player2)
{
	char char_p1 = get_char_player(player1);
	char char_p2 = get_char_player(player2);

	reset_starting_state(da_board, char_p1, char_p2);

	//while (! who_is_the_winner_half(da_board, char_p1, char_p2) && !is_board_full(da_board, char_p1, char_p2)) //to use other end-condition
	while (! is_board_full(da_board, char_p1, char_p2))
	{
		play_turn(da_board, player1, player2, 1);
		play_turn(da_board, player2, player1, 0);
	}

	int winner = who_is_the_winner_half(da_board, char_p1, char_p2);

	return winner;
}

void play(board_t* da_board, player_t* player1, player_t* player2)
{
	int winner = play_game(da_board, player1 ,player2);

	char char_p1 = get_char_player(player1);
	char char_p2 = get_char_player(player2);

	system("clear");
	if (winner == 1)
		printf("THE WINNER IS PLAYER \'%c\'\n\n", char_p1);
	else if (winner == 2)
		printf("THE WINNER IS PLAYER \'%c\'\n\n", char_p2);
	else
		printf("Tie...\n");

	int nb_cells_p1 = how_many_cells_are_this_color(da_board, char_p1);
	int nb_cells_p2 = how_many_cells_are_this_color(da_board, char_p2);
	printf("final board is : \n");
	print_board(da_board);
	print_percentage_occupation(da_board, char_p1, char_p2);
	printf("  (%d - %d)\n", nb_cells_p1, nb_cells_p2);
}

void tournament(player_t* player1, player_t* player2, int nbr_games, int board_size)
{
	int win_player_1 = 0;
	int win_player_2 = 0;

	board_t* da_board = make_board(board_size);

	for(int i = 0; i<nbr_games; i++)
	{
		int temp = play_game(da_board, player1, player2);
		if(temp == 1)
			win_player_1++;
		else if(temp == 2)
			win_player_2++;
	}

	printf("PLAYER 1 (\'%c\') : %d\nPLAYER 2 (\'%c\') : %d\n", get_char_player(player1), win_player_1, get_char_player(player2), win_player_2);

	free_board(da_board);
}
