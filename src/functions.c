#include "../include/functions.h"


void clear_buffer()
{
	char c;
	while ((c = getchar()) != '\n' && c != EOF) { }
}

char read_char_keyboard()
{
	char c = getchar();
	clear_buffer();
	return c;
}

int read_int_from_keyboard()
{
	int c;
	scanf("%d", &c);
	clear_buffer();
	return c;
}

char get_char_from_keyboard_input()
{
	char c = read_char_keyboard();
	if (c > 'A' + NB_COLORS)
		c = c + 'A' - 'a';
	if(c >= 'A' + NB_COLORS || c < 'A')
	{
		printf("nope. retry :");
		return get_char_from_keyboard_input();
	}
	return c;
}

int get_number_between_0_and_x_on_keyboard(int x)
{
	int choice = read_int_from_keyboard();
	if (choice >= 0 && choice <= 0 + x)
		return choice;
	else
		return get_number_between_0_and_x_on_keyboard(x);
}

void print_char_autocolor(char letter, char letter_p1, char letter_p2)
{
	if (letter == letter_p1)
		printf(GREEN_BG BOLD GREEN);
	else if (letter == letter_p2)
		printf(CYAN_BG BOLD CYAN);

	switch (letter)
	{
		case 'A' :
			printf(RED "%c" ANSI_COLOR_RESET, letter);
			break;
		case 'B' :
			printf(GREEN "%c" ANSI_COLOR_RESET, letter);
			break;
		case 'C' :
			printf(YELLOW "%c" ANSI_COLOR_RESET, letter);
			break;
		case 'D' :
			printf(BLUE "%c" ANSI_COLOR_RESET, letter);
			break;
		case 'E' :
			printf(MAGENTA "%c" ANSI_COLOR_RESET, letter);
			break;
		case 'F' :
			printf(CYAN "%c" ANSI_COLOR_RESET, letter);
			break;
		case 'G' :
			printf(WHITE "%c" ANSI_COLOR_RESET, letter);
			break;
		default :
			printf("%c" ANSI_COLOR_RESET, letter);
			break;
	}
}
