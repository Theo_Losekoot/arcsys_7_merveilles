#include "../include/ai.h"

char ask_AI_random()
{
	char letter = rand()%NB_COLORS + 'A';
	return letter;
}

char ask_AI_random_not_stupid(board_t* da_board, char char_player)
{
	int nb_boxes_that_can_be_reach_by_chars[NB_COLORS];
	for(int i=0; i<NB_COLORS; i++)
	{
		nb_boxes_that_can_be_reach_by_chars[i] =
			how_many_boxes_can_be_reached_by_this_char(da_board, char_player, 'A' + i);
	}
	int nb_chars_that_makes_gain_boxes = 0;
	for(int i=0; i<NB_COLORS; i++)
	{
		if(nb_boxes_that_can_be_reach_by_chars[i] > 0)
			nb_chars_that_makes_gain_boxes++;

	}

	char letter = ask_AI_random();

	if (nb_chars_that_makes_gain_boxes == 0)
		return letter;

	//there is at least one, otherwise we could not be here
	while (nb_boxes_that_can_be_reach_by_chars[letter - 'A'] == 0)
	{
		letter = ask_AI_random();
	}

	return letter;

}

char ask_AI_greedy(board_t* da_board, char player_char)
{
	int nb_boxes_that_can_be_reach_by_chars[NB_COLORS];
	for(int i=0; i<NB_COLORS; i++)
	{
		nb_boxes_that_can_be_reach_by_chars[i] =
			how_many_boxes_can_be_reached_by_this_char(da_board, player_char, 'A' + i);
	}
	int best_color = 0;
	for(int i=0; i<NB_COLORS; i++)
	{
		if(nb_boxes_that_can_be_reach_by_chars[i] > nb_boxes_that_can_be_reach_by_chars[best_color])
		{
			best_color = i;
		}
	}
	return best_color + 'A';
}

char ask_AI_thoughtfull_greedy(board_t* da_board, char player_char)
{
	int best_we_did = 0;
	int base_situation = how_many_cells_are_this_color(da_board, player_char);
	int best_color = 0;
	for(int i=0; i<NB_COLORS; i++)
	{
		for(int j=0; j<NB_COLORS; j++)
		{
			board_t* new_board = copy_board(da_board);
			update_world(new_board, player_char, 'A'+i);
			int how_did_we_do_after_first_move = how_many_cells_are_this_color(new_board, player_char);
			// the following condition is there to insure we only update best_color if the considered color has an effect on the board
			if(how_did_we_do_after_first_move > base_situation)
			{
				update_world(new_board, player_char, 'A'+j);
				int how_did_we_do = how_many_cells_are_this_color(new_board, player_char);
				if(how_did_we_do > best_we_did)
				{
					best_we_did = how_did_we_do;
					best_color = i;
				}
			}
			free_board(new_board);
		}
	}
	return best_color + 'A';
}
