#include "../include/board.h"

struct board
{
	char* array;
	int size;
};

board_t* make_board(int size)
{
	board_t* da_board = malloc(sizeof(board_t));
	da_board->size = size;
	da_board->array = malloc( get_size(da_board) * get_size(da_board) * sizeof(char));
	memset(da_board->array, 0, size*size);
	return da_board;
}

void free_board(board_t* da_board)
{
	free (da_board->array);
	free (da_board);
}

board_t* copy_board(board_t* da_board)
{
	int size = get_size(da_board);
	board_t* new_board = make_board(size);
	memcpy(new_board->array, da_board->array, size*size);
	return new_board;
}

int get_size(board_t* da_board)
{
	return da_board->size;
}

/** Retrieves the color of a given board cell */
char get_cell(board_t* da_board, int x, int y)
{
	//assert( (y * get_size(da_board) + x) < get_size(da_board) * get_size(da_board) );
	return da_board->array[y * get_size(da_board) + x];
}

/** Changes the color of a given board cell */
void set_cell(board_t* da_board, int x, int y, char color)
{
	//assert( (y * get_size(da_board) + x) < get_size(da_board) * get_size(da_board) );
	(da_board->array)[y * get_size(da_board) + x] = color;
}

/** Prints the current state of the board on screen
 *
 * Implementation note: It would be nicer to do this with ncurse or even
 * SDL/allegro, but this is not really the purpose of this assignment.
 */
void print_board(board_t* da_board)
{
	char char_p1 = get_cell(da_board, get_size(da_board) -1, 0);
	char char_p2 = get_cell(da_board, 0, get_size(da_board) -1);
	int x, y;
	for (y = 0; y < get_size(da_board); y++)
	{
		for (x = 0; x < get_size(da_board); x++)
		{
			print_char_autocolor(get_cell(da_board, x, y), char_p1, char_p2);
			printf(" ");
		}
		printf("\n");
	}
}

void fill_with_random(board_t* da_board)
{
	int size_board = get_size(da_board);
	for(int i=0; i<size_board*size_board; i++)
	{
		char letter = rand()%NB_COLORS + 'A'; //generates random color
		if (i != size_board-1 && i!= size_board*(size_board-1) )
			set_cell(da_board, i/size_board, i%size_board, letter);
	}
}

void fill_with_random_while_staying_symetrical(board_t* da_board)
{
	int size_board = get_size(da_board);
	for(int i=0; i<size_board; i++)
	{
		for(int j=0; j<=i; j++)
		{
			char letter = rand()%NB_COLORS + 'A';
			set_cell(da_board, i, j, letter);
			set_cell(da_board, j, i, letter);
		}
	}
}

// Prepares the board for a fresh game, generates a new random board 
void reset_starting_state(board_t* da_board, char char_p1, char char_p2)
{
	fill_with_random(da_board);
	set_cell(da_board, get_size(da_board)-1, 0, char_p1);
	set_cell(da_board, 0, get_size(da_board)-1, char_p2);
}

void reset_starting_state_while_staying_symetrical(board_t* da_board, char char_p1, char char_p2)
{
	fill_with_random_while_staying_symetrical(da_board);
	set_cell(da_board, get_size(da_board)-1, 0, char_p1);
	set_cell(da_board, 0, get_size(da_board)-1, char_p2);
}


int cell_exists(board_t* da_board, int x, int y)
{
	return (x>=0 && y >=0 && x<get_size(da_board) && y < get_size(da_board));
}

//declared here because inter-dependance
void update_cell_rec(board_t* da_board, char player, char aimed_char, int x, int y);
void update_cell_if_exists_and_good_color_rec(board_t* da_board, char player, char aimed_char, int x, int y);


/* 	checks if cell exists and if so, checks it's color and if the cell
	is the aimed color, so update the color into palyer's color and re-apply
	the verification to all the cells around.
 */
void update_cell_if_exists_and_good_color_rec(board_t* da_board, char new_color, char aimed_char, int x, int y)
{
	if (	cell_exists(da_board, x, y) &&
			get_cell(da_board, x, y) == aimed_char &&
			get_cell(da_board, x, y) != new_color) //without this, can loop forever if NB_COLORS > 30 (because of symbol '^')
	{
		set_cell(da_board, x, y, new_color);
		update_cell_rec(da_board, new_color, aimed_char, x, y);
	}
}

/*
   changes the color of all cells around the position, if the cell exists and must be changed (is the aimed color).
 */
void update_cell_rec(board_t* da_board, char player, char aimed_char, int x, int y)
{
	update_cell_if_exists_and_good_color_rec(da_board, player, aimed_char, x-1, y);
	update_cell_if_exists_and_good_color_rec(da_board, player, aimed_char, x+1, y);
	update_cell_if_exists_and_good_color_rec(da_board, player, aimed_char, x, y+1);
	update_cell_if_exists_and_good_color_rec(da_board, player, aimed_char, x, y-1);

	//de-comment if you want the 8 boxes around to count.

	//update_cell_if_exists_and_good_color_rec(da_board, player, aimed_char, x-1, y-1);
	//update_cell_if_exists_and_good_color_rec(da_board, player, aimed_char, x+1, y-1);
	//update_cell_if_exists_and_good_color_rec(da_board, player, aimed_char, x+1, y+1);
	//update_cell_if_exists_and_good_color_rec(da_board, player, aimed_char, x-1, y+1);
}

/*
   optimized version of the update function.
   changes all boxes of aimed color that touches the player color into the player color.
 */
void update_world(board_t* da_board, char player, char aimed_char)
{
	for (int y = 0; y < get_size(da_board); y++)
	{
		for (int x = 0; x < get_size(da_board); x++)
		{
			if(get_cell(da_board, x, y) == player)
			{
				update_cell_rec(da_board, player, aimed_char, x, y);
			}
		}
	}
}

/* 	checks if cell exists and if so, checks it's color and if the cell
	is the aimed color, so update the color into palyer's color and re-apply
	the verification to all the cells around.
 */
int update_cell_if_exists_and_good_color_quest2(board_t* da_board, char new_color, char aimed_char, int x, int y)
{
	if (	cell_exists(da_board, x, y) &&
			get_cell(da_board, x, y) == aimed_char &&
			get_cell(da_board, x, y) != new_color) //without this, can loop forever if NB_COLORS > 30 (because of symbol '^')
	{
		set_cell(da_board, x, y, new_color);
		return 1;
	}
	return 0;
}

void update_world_quest2(board_t* da_board, char player, char aimed_char)
{
	int modif = 0;
	for (int y = 0; y < get_size(da_board); y++)
	{
		for (int x = 0; x < get_size(da_board); x++)
		{
			if(get_cell(da_board, x, y) == player)
			{
				modif += update_cell_if_exists_and_good_color_quest2(da_board, player, aimed_char, x-1, y);
				modif += update_cell_if_exists_and_good_color_quest2(da_board, player, aimed_char, x+1, y);
				modif += update_cell_if_exists_and_good_color_quest2(da_board, player, aimed_char, x, y-1);
				modif += update_cell_if_exists_and_good_color_quest2(da_board, player, aimed_char, x, y+1);
			}

		}
	}
	if (modif)
		update_world_quest2(da_board, player, aimed_char);
}

/*	returns true only is grid is fully filled by player1 and player2   */
int is_board_full(board_t* da_board, char player1, char player2)
{
	for (int y = 0; y < get_size(da_board); y++)
	{
		for (int x = 0; x < get_size(da_board); x++)
		{
			if(get_cell(da_board, x, y) != player1 && get_cell(da_board, x, y) != player2)
			{
				return 0;
			}
		}
	}
	return 1;
}

int how_many_cells_are_this_color(board_t* da_board, char color)
{
	int number = 0;
	for (int y = 0; y < get_size(da_board); y++)
	{
		for (int x = 0; x < get_size(da_board); x++)
		{
			if(get_cell(da_board, x, y) == color)
				number++;
		}
	}
	return number;
}

double percentage_owned_by_color(board_t* da_board, char color)
{
	int nb_boxes = how_many_cells_are_this_color(da_board, color);
	int size = get_size(da_board) * get_size(da_board);
	return ( (100.0 * nb_boxes) / size );
}

void print_percentage_occupation(board_t* da_board, char char_p1, char char_p2)
{
	double occupation_p1 = percentage_owned_by_color(da_board, char_p1);
	double occupation_p2 = percentage_owned_by_color(da_board, char_p2);
	printf("Occupations : \n");
	print_char_autocolor(char_p1, char_p1, char_p2);
	printf(" : %lf %%\n", occupation_p1);
	print_char_autocolor(char_p2, char_p1, char_p2);
	printf(" : %lf %%\n", occupation_p2);
}

int how_many_boxes_can_be_reached_by_this_char(board_t* da_board, char char_player, char aimed_char)
{
	board_t* new_board = copy_board(da_board);

	int before = how_many_cells_are_this_color(new_board, char_player);
	update_world(new_board, char_player, aimed_char);
	int after = how_many_cells_are_this_color(new_board, char_player);

	free_board(new_board);

	return after - before;
}

/**
  returns	0 if nobody wins,
  1 if player 1 wins and
  2 if player2 wins
 **/
int who_is_the_winner_half(board_t* da_board, char player1, char player2)
{
	int how_many_boxes_p1 = how_many_cells_are_this_color(da_board, player1);
	int how_many_boxes_p2 = how_many_cells_are_this_color(da_board, player2);

	int half_board = (get_size(da_board) * get_size(da_board) / 2);

	if (how_many_boxes_p1 > half_board)
		return 1;
	else if (how_many_boxes_p2 > half_board)
		return 2;
	else
		return 0;
}

/**
  returns	0 if nobody wins,
  1 if player 1 wins and
  2 if player2 wins
 **/
int who_is_the_winner_full(board_t* da_board, char player1, char player2)
{
	if(is_board_full(da_board, player1, player2))
		return (who_is_the_winner_half(da_board, player1, player2));
	else
		return 0;
}
