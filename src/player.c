#include "../include/player.h"

struct player
{
	char character;
	int role;
};

player_t* make_player(char char_player, int da_role)
{
	player_t* da_player = malloc(sizeof(player_t));
	da_player->character = char_player;
	da_player->role = da_role;
	return da_player;
}

void free_player(player_t* da_player)
{
	free(da_player);
}

int get_role_player(player_t* da_player)
{
	return da_player->role;
}

char get_char_player(player_t* da_player)
{
	return da_player->character;
}


char ask_player(board_t* da_board, char char_player, char char_opponent, int is_player1_turn)
{
	system("clear");
	printf("\nplayer \'%c\', it's your turn ! \n", char_player);
	printf("The actual board is like that : \n");
	print_board(da_board);
	if(is_player1_turn)
		print_percentage_occupation(da_board, char_player, char_opponent);
	else
		print_percentage_occupation(da_board, char_opponent, char_player);

	printf("Enter the color you want to play : ");
	char choice = get_char_from_keyboard_input();
	return choice;
}



void play_turn(board_t* da_board, player_t* da_player, player_t* da_opponent, int is_player1_turn)
{

	char what_to_play;
	if (get_role_player(da_player) == HUMAN)
	{
		what_to_play = ask_player(da_board, get_char_player(da_player), get_char_player(da_opponent), is_player1_turn);
	}
	else if(get_role_player(da_player) == AI_RANDOM)
	{
		what_to_play = ask_AI_random();
	}
	else if(get_role_player(da_player) == AI_RANDOM_NOT_STUPID)
	{
		what_to_play = ask_AI_random_not_stupid(da_board, get_char_player(da_player));
	}
	else if(get_role_player(da_player) == AI_GREEDY)
	{
		what_to_play = ask_AI_greedy(da_board, get_char_player(da_player));
	}
	else if(get_role_player(da_player) == AI_BETTER_GREEDY)
	{
		what_to_play = ask_AI_thoughtfull_greedy(da_board, get_char_player(da_player));
	}

	update_world(da_board, get_char_player(da_player), what_to_play);
}

void show_possible_roles()
{
	printf(" 0 : HUMAN \n 1 : AI_RANDOM \n 2 : AI_RANDOM_NOT_STUPID \n 3 : AI_GREEDY \n 4 : AI_BETTER_GREEDY \n");
}
